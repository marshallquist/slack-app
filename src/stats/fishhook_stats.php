#!/usr/bin/php
<?php

if(file_exists('/var/www/noc/includes')) {
	$include_dir = '/var/www/noc/includes';
}
elseif(file_exists('/var/www/noc_production/includes')) {
	$include_dir = '/var/www/noc_production/includes';
}
else {
	die('Could not find NOC360 include directory');
}
if(file_exists('/var/www/nocadmin/includes')) {
	$admin_include_dir = '/var/www/nocadmin/includes';
}
elseif(file_exists('/var/www/nocadmin_production/includes')) {
	$admin_include_dir = '/var/www/nocadmin_production/includes';
}
else {
	die('Could not find NOC360 include directory');
}
require_once($include_dir . '/config.inc.php');
require_once($include_dir . '/modules/General.helpers.php');
require_once($include_dir . '/modules/Debug.helpers.php');
require_once($include_dir . '/modules/MySQL.class.php');
require_once($include_dir . '/modules/phpMailer/class.phpmailer.php');
require_once($admin_include_dir . '/modules/pChart/class/pData.class.php');
require_once($admin_include_dir . '/modules/pChart/class/pDraw.class.php');
require_once($admin_include_dir . '/modules/pChart/class/pImage.class.php');
require_once($admin_include_dir . '/dbaccess/fishhook_stats.class.php');
require_once($admin_include_dir . '/dbaccess/instance.class.php');
require_once($include_dir . '/dbaccess/company.class.php');

$days_to_display = 14;
$GlobalAdmin = new MySQL($GLOBAL_DB_HOSTS['Global']);
unset($DB_HOSTS);
$base_include_path = get_include_path();
$temp_dir = sys_get_temp_dir();

fishhook_stats::clean_up();
$instances = fishhook_stats::fetch_instances();
$graph_files = array();
$email_body = '<html><body>';
foreach($instances as $instance_id => $instance_name) {
	# Reset the config vars to the base default values
	# Please note that variables defined in any previous instance's config that are not in the base config will persist
	require($include_dir . '/config.inc.php');
	unset($DB_HOSTS);
	$instance = instance::fetch($instance_id);
	$instance_include_path = $INSTANCE_CONFIG_PATH . '/' . $instance['include_path'];
	if(file_exists($instance_include_path)) {
		set_include_path($base_include_path . PATH_SEPARATOR . $instance_include_path);
		if(file_exists($instance_include_path . '/' . 'instance_config.inc.php')) {
			require_once $instance_include_path . '/' . 'instance_config.inc.php';
			$INSTANCE_DIR = $INSTANCE_CONFIG_PATH . '/'. $instance['include_path'];
			$NOC = new MySQL($DB_HOSTS['NOC']);
			$companies = fishhook_stats::fetch_companies($instance_id);
			foreach($companies as $company_id) {
				$company = company::fetch($company_id);
				$import_types = fishhook_stats::fetch_import_types($instance_id, $company_id);
				$data_points = array();
				$tmp_image_name = tempnam($temp_dir, 'fh_stats_' . $instance_id . '_' . $company_id . '.png');
				$graph_files[$tmp_image_name] = 'fh_stats_' . $instance_id . '_' . $company_id . '.png';
				$total_data_points = array();
				$service_count = 0;
				foreach($import_types as $import_type) {
					if($import_type != 'subscriber') {
						$service_count++;
					}
					$gen_date = new DateTime();
					$today = $gen_date->format('Y-m-d');
					$fh_stats = fishhook_stats::fetch_all($instance_id, $company_id, $import_type);
					$rev_data_points = array();
					$rev_data_dates = array();
					for($i = 0; $i < $days_to_display; $i++) {
						$found = false;
						foreach($fh_stats as $fh_stat) {
							if($fh_stat->import_date == $today) {
								$rev_data_points[] = $fh_stat->total_imported;
								$rev_data_dates[] = $fh_stat->import_date;
								if($import_type != 'subscriber') {
									if(array_key_exists($fh_stat->import_date, $total_data_points)) {
										$total_data_points[$fh_stat->import_date] += $fh_stat->total_imported;
									}
									else {
										$total_data_points[$fh_stat->import_date] = $fh_stat->total_imported;
									}
								}
								$found = true;
								break;
							}
						}
						if(!$found) {
							$rev_data_points[] = VOID;
							$rev_data_dates[] = $today;
						}
						$gen_date = new DateTime($today);
						$gen_date->sub(new DateInterval('P1D'));
						$today = $gen_date->format('Y-m-d');
					}
					$data_points[$import_type] = array_reverse($rev_data_points);
					$data_dates = array_reverse($rev_data_dates);
				}
				$MyData = new pData();
				if(($instance['license_count'] != 0) and ($service_count > 1)) {
					$total_graph_points = array();
					foreach($data_dates as $sample_date) {
						if(!array_key_exists($sample_date, $total_data_points)) {
							$total_graph_points[] = VOID;
						}
						else {
							$total_graph_points[] = $total_data_points[$sample_date];
						}
					}
					$MyData->addPoints($total_graph_points, 'Total');
					$MyData->setPalette('Total', array('R' => 255, 'G' => 0, 'B' => 0));
					$MyData->setSerieDrawable("Total", false);
				}
				foreach($data_points as $type => $series) {
					$MyData->addPoints($series, ucfirst($type));
					switch($type) {
						case 'subscriber':
							$MyData->setPalette('Subscriber', array('R' => 55, 'G' => 91, 'B' => 127));
							break;
						case 'wired':
							$MyData->setPalette('Wired', array('R' => 177, 'G' => 66, 'B' => 244));
							break;
						case 'wireless':
							$MyData->setPalette('Wireless', array('R' => 131, 'G' => 244, 'B' => 66));
							break;
						case 'cable':
							$MyData->setPalette('Cable', array('R' => 244, 'G' => 122, 'B' => 66));
							break;
					}
				}
				$MyData->addPoints($data_dates, 'Dates');
				$MyData->setAbscissa('Dates');
				# Create the pChart object
				$myPicture = new pImage(750, 270, $MyData);
				# Turn off Antialiasing
				$myPicture->Antialias = FALSE;
				# Draw the background
				$Settings = array("R" => 255, "G" => 255, "B" => 255);
				$myPicture->drawFilledRectangle(0, 0, 750, 230, $Settings);
				# Set Title Background
				$myPicture->drawGradientArea(0, 0, 750, 20, DIRECTION_HORIZONTAL, array("StartR" => 55, "StartG" => 91, "StartB" => 127, "Levels" => 50, "Alpha" => 100));
				# Add a border to the picture
				$myPicture->drawRectangle(0, 0, 749, 269, array("R"=>0, "G"=>0, "B"=>0));
				# Write the chart title
				$myPicture->setFontProperties(array("FontName" => "../includes/modules/pChart/fonts/Bedizen.ttf", "FontSize" => 12, "R" => 255, "G" => 255, "B" => 255));
				$myPicture->drawText(10, 18, $company->name, array("FontSize" => 12, "Align" => TEXT_ALIGN_BOTTOMLEFT));
				# Set the default font
				$myPicture->setFontProperties(array("FontName" => "../includes/modules/pChart/fonts/Bedizen.ttf", "FontSize" => 10, "R" => 0, "G" => 0, "B" => 0));
				# Define the chart area
				$myPicture->setGraphArea(60, 40, 650, 200);
				# Draw the scale
				$scaleSettings = array("XMargin" => 10, "YMargin" => 10, "Floating" => TRUE, "GridR" => 200, "GridG" => 200, "GridB" => 200, "DrawSubTicks" => FALSE, "CycleBackground" => TRUE, "LabelRotation" => 45);
				$myPicture->drawScale($scaleSettings);
				# Turn on Antialiasing
				$myPicture->Antialias = TRUE;
				# Draw the line chart
				$myPicture->drawLineChart();
				# Put the data points on the chart
				$myPicture->drawPlotChart(array("DisplayValues" => TRUE, "PlotBorder" => FALSE, "BorderSize" => 1, "Surrounding" => -60, "BorderAlpha" => 80));
				if(($instance['license_count'] != 0) and ($service_count > 1)) {
					foreach($data_points as $type => $series) {
						$MyData->setSerieDrawable($type, false);
					}
					$MyData->setSerieDrawable("Total", true);
					$myPicture->drawLineChart(array("DisplayValues" => false));
				}
				$myPicture->drawLegend(670, 35, array("Style" => LEGEND_NOBORDER, "Mode" => LEGEND_VERTICAL, "FontR" => 0, "FontG" => 0, "FontB" => 0));
				# if there is a license limit, put a threshold line on the graph indicating the max limit
				if($instance['license_count'] != 0) {
						$myPicture->drawThreshold($instance['license_count'], array('Alpha' => 70, 'Ticks' => 1));
				}
				# Render the picture (choose the best way)
				$myPicture->autoOutput($tmp_image_name);
				$email_body .= '<img src="cid:' . $graph_files[$tmp_image_name] . '"><br>';
			}
		}
	}
}
$email_body .= '</body></html>';
$mail = new PHPMailer();
$mail->IsHTML(true);
$mail->SetFrom($EMAIL_FROM, 'NOC360');
$mail->AddAddress('dev@7sigma.com', '7Sigma Development Team');
$mail->AddAddress('dpetersen@7sigma.com', 'Daphne Petersen');
#$mail->AddAddress('bblix@7sigma.com', 'Brad Blix');
$mail->Subject = 'Fish Hook Import Stats';
foreach($graph_files as $file_path => $file_name) {
	$mail->AddEmbeddedImage($file_path, $file_name);
}
$mail->Body = $email_body;
$mail->Send();
foreach($graph_files as $file_path => $file_name) {
	unlink($file_path);
}

?>