<?php
$slack_webhook_url = "https://hooks.slack.com/services/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";
$r2 = $_POST['payload'];
//get yes or no value
$val = $r2[81];


//replace text with space for no
$json_string='{
    "text": " ",
    "response_type": "ephemeral",
    "replace_original": true,
    "delete_original": true
}';

//replace with resolved for yes
$json_string2='{
    "text": "Resolved",
    "response_type": "ephemeral",
    "replace_original": true,
    "delete_original": true
}';

//Delete the message if "no" is selected

if ($val == 'n') {
    $slack_call = curl_init($slack_webhook_url);
    curl_setopt($slack_call, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($slack_call, CURLOPT_POSTFIELDS, $json_string);
    curl_setopt($slack_call, CURLOPT_CRLF, true);
    curl_setopt($slack_call, CURLOPT_RETURNTRANSFER, false);
    curl_setopt(
    $slack_call,
    CURLOPT_HTTPHEADER,
    array(
    "Content-Type: application/json",
    "Content-Length: " . strlen($json_string))
);

    $result = curl_exec($slack_call);
    curl_close($slack_call);
}

if ($val == 'y') {
    $slack_call = curl_init($slack_webhook_url);
    curl_setopt($slack_call, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($slack_call, CURLOPT_POSTFIELDS, $json_string2);
    curl_setopt($slack_call, CURLOPT_CRLF, true);
    curl_setopt($slack_call, CURLOPT_RETURNTRANSFER, false);
    curl_setopt(
        $slack_call,
        CURLOPT_HTTPHEADER,
        array(
        "Content-Type: application/json",
        "Content-Length: " . strlen($json_string2))
    );
    
    $result = curl_exec($slack_call);
    curl_close($slack_call);
}
