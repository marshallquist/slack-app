<?php

/**
  * Open a connection via PDO to create a
  * new database and table with structure.
  *
  */

require "config.php";

try {
    $connection = new PDO($dsn, $username, $password, $options);
    $sql = file_get_contents("stats/global_noc_fishbook_stats.sql");
    $connection->exec($sql);

    echo "Values added";
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
