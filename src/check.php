<?php

//set up global variables for url creation
global $name;
global $url;
$urlarray = array();




// function to make graphs
function make_img($arr, $n)
{
    header("Content-type: image/png");

    $arrval = array_column($arr, 'total_imported');

    $name = $n;
    //$name = strval($arr[0]['fishhook_id']);

    $height = 260;

    $width = 330;

    $im = imagecreate($width, $height);

    $white = imagecolorallocate($im, 255, 255, 255);

    $gray = imagecolorallocate($im, 200, 200, 200);

    $black = imagecolorallocate($im, 0, 0, 0);

    $red = imagecolorallocate($im, 255, 0, 0);

    $green = imagecolorallocate($im, 0, 255, 0);

    $blue = imagecolorallocate($im, 0, 0, 255);

    $x = 21;

    $y = 11;

    $num = 0;

    $color = $black;

    //color code graphs
    if ($arr[0]['import_type'] == 'wired') {
        $color = $blue;
    }
    if ($arr[0]['import_type'] == 'subscriber') {
        $color = $red;
    }
    if ($arr[0]['import_type'] == 'wireless') {
        $color = $green;
    }

    //make grid for graph
    while ($x<=$width && $y<=$height) {
        $prcnt = (((($height-50)-($y-1))/($height-60))*100)*2;

        imageline($im, 21, $y, $width-10, $y, $gray);

        imageline($im, $x, 11, $x, $height-50, $gray);

        imagestring($im, 1, 1, $y-10, $prcnt.'00', $black);

        if (($num%2)==0) {
            imagestring($im, 0, $x-20, $height-42, $arr[$num]['import_date'], $black);
        }
        if (($num%2)==1) {
            imagestring($im, 0, $x-20, $height-32, $arr[$num]['import_date'], $black);
        }
        $x += 30;

        $y += 20;

        $num++;
    }

    $tx = 20;

    $ty = 210 - $arrval[0]/100;

    //graph points
    foreach ($arrval as $values) {
        $cx = $tx + 30;

        $cy = 210 - $values/100;

        imageline($im, $tx, $ty, $cx, $cy, $color);

        imagestring($im, 5, $cx-3, $cy-13, '.', $color);

        $ty = $cy;

        $tx = $cx;
    }

    //axis
    imageline($im, 20, 11, 20, $height-50, $black);

    imageline($im, 20, $height-49, $width-10, $height-49, $black);

    imagestring($im, 2, 10, $height-20, 'Company-id:'.$arr[0]['company_id'].', Import-type:'.$arr[0]['import_type']. ', Instance-id:'. $arr[0]['instance_id'], $color);


    $save = "/app/src/".$name.".png";

    imagepng($im, $save);
}





////////////////////////////





//look into past function
function subDay($date)
{
    return date('Y-m-d', strtotime($date. ' - 1 days'));
}
//calculate percent difference
function percentdiff($x, $y)
{
    $diff = abs($x-$y);
    $avg = ($x + $y) / 2;
    $per = $diff / $avg;
    return $per;
}

try {
    require "config.php";

    $connection = new PDO($dsn, $username, $password, $options);

    //collect all data from db
    $sql = "SELECT * FROM fishhook_stats";
    $statement = $connection->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_ASSOC);


    //make seperate arrays for all columns
    $size = sizeof($result);
    $fishhook_id = array_column($result, 'fishhook_id');
    $instance_id = array_column($result, 'instance_id');
    $company_id = array_column($result, 'company_id');
    $import_type = array_column($result, 'import_type');
    $total_imported = array_column($result, 'total_imported');

    
    //these arrays will be to store indexes of points
    $marker = array();
    $marker2 = array();
    


    for ($i=0; $i<$size; $i++) {
        //get current data values
        $id = $fishhook_id[$i];
        $inst = $instance_id[$i];
        $comp = $company_id[$i];
        $type = $import_type[$i];
        $total = $total_imported[$i];
        $date = $result[$i]['import_date'];
        $past = subDay($date);
        for ($f = 0; $f<$i; $f++) {
            //check if there are any values in the past from the same company and instance
            if ($instance_id[$f] == $inst and $company_id[$f] == $comp and $import_type[$f] == $type and $result[$f]['import_date'] == $past) {
                if ($result[end($marker)]['instance_id'] == $result[$f]['instance_id'] and $result[end($marker)]['import_type'] == $result[$f]['import_type'] and $result[end($marker)]['company_id'] == $result[$f]['company_id']) {
                    //ignore repeats
                    continue;
                }
                //check if the difference is big enough
                $var = percentdiff($total, $total_imported[$f]);
                if ($var > .2) {
                    array_push($marker, $i);
                    array_push($marker2, $f);
                }
                break;
            }
        }
    }
    


    //generate points
    for ($m=0; $m<sizeof($marker); $m++) {
        $x = array();
        $amt = array();
        $val;
        for ($g=0; $g<$size; $g++) {
            //get all array entries that match selected entry
            if ($instance_id[$g] == $instance_id[$marker[$m]] and $company_id[$g] == $company_id[$marker[$m]] and $import_type[$g] == $import_type[$marker[$m]]) {
                array_push($amt, $result[$g]);
                if ($result[$g] == $result[$marker[$m]]) {
                    $val = $result[$g];
                }
            }
        }
        //select 5 on each side of selected entry to graph with
        for ($h=0; $h<sizeof($amt); $h++) {
            if ($amt[$h] == $val) {
                for ($r=-5; $r<=5; $r++) {
                    if ($h+$r > sizeof($amt)) {
                        break;
                    }
                    if ($h+$r < 0) {
                        continue;
                    }
                    array_push($x, $amt[$h + $r]);
                }
            }
        }
        //create image and image url
        $name = strval($x[0]['fishhook_id']);
        make_img($x, $name);
        $url = strval("https://interactive-prototype.herokuapp.com/src/$name.png");
        array_push($urlarray, $url);

        unset($x);
        unset($amt);
    }
    
    //loop through array of urls and send messages with images
    for ($v=0; $v<sizeof($urlarray); $v++) {
        $slack_webhook_url = "https://hooks.slack.com/services/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";
        $json_message = '{
            "text": "Is this a problem?",
            "attachments": [
                {
                    "fallback": "You are unable to choose",
                    "callback_id": "error",
                    "color": "#3AA3E3",
                    "attachment_type": "default",
                    "image_url": "'.$urlarray[$v].'",
                    "actions": [
                        {
                            "name": "game",
                            "text": "Yes",
                            "type": "button",
                            "value": "yes"
                        },
                 
                        {
                            "name": "game",
                            "text": "No",
                            "style": "danger",
                            "type": "button",
                            "value": "no",
                            "confirm": {
                                "title": "Are you sure?",
                                "text": "Are you sure?",
                                "ok_text": "Yes",
                                "dismiss_text": "No"
                            }
                        }
                    ]
                }
            ]
        }';


        $slack_call = curl_init($slack_webhook_url);
        curl_setopt($slack_call, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($slack_call, CURLOPT_POSTFIELDS, $json_message);
        curl_setopt($slack_call, CURLOPT_CRLF, false);
        curl_setopt($slack_call, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $slack_call,
            CURLOPT_HTTPHEADER,
            array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($json_message))
        );

        $result = curl_exec($slack_call);
        curl_close($slack_call);
    }
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
