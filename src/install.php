<?php

/**
  * Open a connection via PDO to create a
  * new database and table with structure.
  *
  */

require "config.php";

try {
    $connection = new PDO($dsn, $username, $password, $options);
    /*$sql = file_get_contents("slackapp/src/init.sql");
    $connection->exec($sql);*/

    echo "Database created successfully.";
} catch (PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}
